#!/bin/sh

set -e

case "$CI_PROJECT_PATH" in
  morello/manifest)
    export MANIFEST_BRANCH=${MANIFEST_BRANCH:-"${CI_COMMIT_BRANCH}"}
  ;;
  morello/llvm-project)
    export LLVM_PROJECT_BRANCH=${LLVM_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/build-scripts)
    export BUILD_SCRIPTS_BRANCH=${BUILD_SCRIPTS_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/device/arm/morello)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
esac

case "$CI_PROJECT_PATH" in
  */morello-ci-pipelines)
    echo "Skip Morello CI pipelines"
  ;;
  *)
    echo "Get Morello CI pipelines (master)"
    curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo /tmp/ci-pipelines.tar.bz2 \
      https://git.morello-project.org/morello/morello-ci-pipelines/-/archive/master/morello-ci-pipelines-master.tar.bz2 ;\
    tar -xf /tmp/ci-pipelines.tar.bz2 \
      morello-ci-pipelines-master/.gitlab-ci \
      morello-ci-pipelines-master/lava \
      --strip-components=1
  ;;
esac

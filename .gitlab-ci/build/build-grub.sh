#!/bin/sh

set -e

sudo apt update -q=2
sudo apt install -q=2 --yes --no-install-recommends grub-efi-arm64

set -x

touch grub.configfile
grub-mkimage \
  --config=grub.configfile \
  --output=grubaa64.efi \
  --format=arm64-efi \
  --prefix /EFI/BOOT \
  --verbose \
  boot chain configfile efinet ext2 fat gettext help linux loadenv lsefi normal part_gpt part_msdos read search search_fs_file search_fs_uuid search_label terminal terminfo tftp time

sha256sum grub*.efi > SHA256SUMS.txt

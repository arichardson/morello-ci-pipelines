#!/bin/sh

#git submodule sync --recursive
#git submodule update --init --recursive

set -ex

export WORKSPACE=${CI_BUILDS_DIR}/${CI_PROJECT_NAMESPACE}
export PACKAGES_PATH=${WORKSPACE}/edk2:${WORKSPACE}/edk2-platforms
git clone --depth 1 https://git.morello-project.org/morello/edk2-platforms.git ${WORKSPACE}/edk2-platforms
cd ${WORKSPACE}
source edk2/edksetup.sh
make -C edk2/BaseTools -j $(nproc)
CLANG38_AARCH64_PREFIX=llvm- \
  build \
    -a AARCH64 \
    -b DEBUG \
    -t CLANG38 \
    -n 0 \
    -p Platform/ARM/Morello/MorelloPlatformFvp.dsc \
    -D ENABLE_MORELLO_CAP

cd ${CI_PROJECT_DIR}
mv ${WORKSPACE}/Build/morellofvp/DEBUG_CLANG38/FV/BL33_AP_UEFI.fd .
# Create SHA256SUMS.txt file
sha256sum BL33_AP_UEFI.fd > SHA256SUMS.txt

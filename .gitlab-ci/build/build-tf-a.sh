#!/bin/sh

TC_DIR=${TC_DIR:-"${HOME}/custom-tc/bin"}
TC_URL=${TC_URL:-}

install_custom_toolchain()
{
  test -d ${TC_DIR} && return 0
  test -z "${TC_URL}" && return 0
  TC="/tmp/morello-clang.tar.xz"
  test -f ${TC} || curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo ${TC} ${TC_URL}
  mkdir -p $(dirname ${TC_DIR})
  tar -xf ${TC} -C $(dirname ${TC_DIR}) --strip-components=1
  export PATH="${TC_DIR}:${PATH}"
  printf "Custom toolchain installed from %s\n" "${TC_URL}"
  which clang
  clang --version
}

install_custom_toolchain

which aarch64-none-elf-gcc
aarch64-none-elf-gcc --version

if [ ${CI_JOB_NAME} = "test-tf-a" ]; then
  WORKSPACE=${CI_BUILDS_DIR}/${CI_PROJECT_NAMESPACE}
  mkdir -p ${WORKSPACE}
  git clone --depth 1 https://git.morello-project.org/morello/trusted-firmware-a.git ${WORKSPACE}/tf-a
  cd ${WORKSPACE}/tf-a
fi

set -ex

CROSS_COMPILE=aarch64-none-elf- \
  make -j $(nproc) \
  V=1 \
  PLAT=morello \
  DEBUG=1 \
  CC=clang \
  CFLAGS="-Wno-unused-function" \
  LD=ld.lld \
  ARCH=aarch64 \
  OC=llvm-objcopy \
  OD=llvm-objdump \
  ENABLE_MORELLO_CAP=1 \
  bl31 dtbs

cp -a \
  build/morello/debug/bl31.bin \
  build/morello/debug/fdts/*.dtb \
  ${CI_PROJECT_DIR}

cd ${CI_PROJECT_DIR}
# Create SHA256SUMS.txt file
sha256sum bl31.bin *.dtb > SHA256SUMS.txt

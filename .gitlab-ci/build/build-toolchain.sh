#!/bin/sh

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"
LLVM_PROJECT_BRANCH="${LLVM_PROJECT_BRANCH:-morello/master}"
CHECK_TESTS="${CHECK_TESTS:-"check check-clang check-lld check-lldb"}"

if [ -z "${ANDROID_GIT_COOKIE}" ]; then
  printf "Skip http.cookiefile\n"
else
  printf "android.googlesource.com\tFALSE\t/\tTRUE\t2147483647\to\t${ANDROID_GIT_COOKIE}\n" > ~/.gitcookies
  chmod 0600 ~/.gitcookies
  git config --global http.cookiefile ~/.gitcookies
  printf "Set http.cookiefile\n"
fi

set -ex

repo init --no-clone-bundle \
  -u https://git.morello-project.org/morello/manifest.git \
  -b ${MANIFEST_BRANCH} -g toolchain-src

# Set the llvm-project revision
xmlstarlet edit --inplace \
  --update "//project[@name='llvm-project']/@revision" \
  --value ${LLVM_PROJECT_BRANCH} \
  .repo/manifests/morello-toolchain.xml

# Avoid to download +12G of prebuilt binaries
sed -i '/darwin/d' .repo/manifests/morello-toolchain.xml
sed -i '/mingw32/d' .repo/manifests/morello-toolchain.xml
sed -i '/windows/d' .repo/manifests/morello-toolchain.xml
xmlstarlet edit --inplace  \
  --update "//remote[@name='aosp']/@fetch" \
  --value "https://android.googlesource.com/a/" \
  .repo/manifests/remotes.xml

repo sync -j8 --quiet --no-clone-bundle
repo manifest -r -o pinned-manifest.xml
cat pinned-manifest.xml

export PATH=${CI_PROJECT_DIR}/toolchain-src/prebuilts/build-tools/linux-x86/bin:${CI_PROJECT_DIR}/toolchain-src/prebuilts/cmake/linux-x86/bin:${PATH}
which cmake
cmake --version

./toolchain-src/toolchain/llvm_android/build.py --install-llvm-utils --no-build windows
cp -a toolchain-src/out/install/linux-x86/clang-dev ${CI_PROJECT_DIR}/clang-current
cp -a toolchain-src/out/stage2-install/bin/FileCheck ${CI_PROJECT_DIR}/clang-current/bin
# Compress the toolchain
cd ${CI_PROJECT_DIR}
time tar -cJf ${CI_PROJECT_DIR}/morello-clang.tar.xz clang-current
# Create SHA256SUMS.txt file
sha256sum *.tar.xz *.xml > SHA256SUMS.txt

cd ${CI_PROJECT_DIR}/toolchain-src/out/stage2
LD_LIBRARY_PATH=${CI_PROJECT_DIR}/toolchain-src/out/stage2/lib64:${CI_PROJECT_DIR}/toolchain-src/out/stage2-install/lib64:${CI_PROJECT_DIR}/toolchain-src/out/lib/libxml2-linux \
PYTHONHOME=${CI_PROJECT_DIR}/toolchain-src/prebuilts/python/linux-x86 \
  ninja ${CHECK_TESTS}

# Pass variables to test job
echo "TC_URL=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/jobs/${CI_JOB_ID}/artifacts/morello-clang.tar.xz" >> ${CI_PROJECT_DIR}/build.env

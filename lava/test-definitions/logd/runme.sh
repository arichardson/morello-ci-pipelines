#!/bin/sh

set +x

curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo userdata.tar.xz "${USERDATA_URL}"
tar xf userdata.tar.xz

for test in "liblog-unit-tests" "logd-unit-tests"; do
  for test_path in "nativetest64" "nativetestc64"; do
    adb push "data/${test_path}/${test}/${test}" \
      "/data/${test_path}/${test}/${test}"
    adb shell "/data/${test_path}/${test}/${test}" 2>&1 \
      | tee /tmp/results.log

    grep "\[==========\]" /tmp/results.log
    grep "\[  PASSED  \]" /tmp/results.log
    grep "\[  SKIPPED \]" /tmp/results.log
    grep "\[  FAILED  \]" /tmp/results.log

    grep -q "\[  FAILED  \]" /tmp/results.log
    status=$?
    echo "<LAVA_SIGNAL_STARTTC ${test_path}-${test}>"
    if [ "$status" -eq 1 ]; then
      echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test_path}-${test} RESULT=pass>"
    else
      echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test_path}-${test} RESULT=fail>"
    fi
    echo "<LAVA_SIGNAL_ENDTC ${test_path}-${test}>"
    rm -f /tmp/results.log
  done
done

test="liblog-benchmarks"
for test_path in "benchmarktest64" "benchmarktestc64"; do
  adb push "data/${test_path}/${test}/${test}" \
    "/data/${test_path}/${test}/${test}"
  adb shell "/data/${test_path}/${test}/${test} --benchmark_repetitions=1 --benchmark_min_time=0.001" 2>&1 \
    | tee /tmp/results.log

  # FIXME: parse benchmark test results
  status=$?
  echo "<LAVA_SIGNAL_STARTTC ${test_path}-${test}>"
  if [ "$status" -eq 0 ]; then
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test_path}-${test} RESULT=pass>"
  else
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test_path}-${test} RESULT=fail>"
  fi
  echo "<LAVA_SIGNAL_ENDTC ${test_path}-${test}>"
  rm -f /tmp/results.log
done

#!/bin/sh

set +x

curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo userdata.tar.xz "${USERDATA_URL}"
tar xf userdata.tar.xz

TESTS="binderDriverInterfaceTest binderLibTest binderSafeInterfaceTest"
TESTS="${TESTS} binderTextOutputTest binderValueTypeTest"

for test in ${TESTS}; do
  adb push "data/nativetest64/${test}/${test}" \
    "/data/nativetest64/${test}/${test}"

  case "${test}" in
    binderSafeInterfaceTest)
      adb shell "ulimit -n 128 && /data/nativetest64/${test}/${test}" 2>&1 \
        | tee /tmp/results.log
      ;;
    *)
      adb shell "ulimit -n 32768 && /data/nativetest64/${test}/${test}" 2>&1 \
        | tee /tmp/results.log
      ;;
  esac

  grep "\[==========\]" /tmp/results.log
  grep "\[  PASSED  \]" /tmp/results.log
  grep "\[  SKIPPED \]" /tmp/results.log
  grep "\[  FAILED  \]" /tmp/results.log

  grep -q "\[  FAILED  \]" /tmp/results.log
  status=$?
  echo "<LAVA_SIGNAL_STARTTC ${test}>"
  if [ "$status" -eq 1 ]; then
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test} RESULT=pass>"
  else
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test} RESULT=fail>"
  fi
  echo "<LAVA_SIGNAL_ENDTC ${test}>"
  rm -f /tmp/results.log
done

test="binderThroughputTest"
adb push "data/nativetest64/${test}/${test}" \
  "/data/nativetest64/${test}/${test}"
adb shell "ulimit -n 32768 && /data/nativetest64/${test}/${test} -i 1000 -p" 2>&1 \
  | tee /tmp/results.log
iterations=$(grep "iterations" /tmp/results.log | awk -F ':' '{ gsub(/ /,""); print $2 }')
echo "<LAVA_SIGNAL_STARTTC ${test}>"
if [ ${iterations} -gt 100000 ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test} RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test} RESULT=fail>"
fi
echo "<LAVA_SIGNAL_ENDTC ${test}>"
rm -f /tmp/results.log
